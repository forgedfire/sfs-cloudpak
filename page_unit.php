<?php
	$getUnit = mysql_query('SELECT * FROM unit WHERE id = '.$_GET['id'].'');
	if(mysql_num_rows($getUnit) > 0) {
		global $unit;
		$unit = mysql_fetch_assoc($getUnit);
	} else {
		global $retrievalError;
		$retrievalError = true;
	}

	function title() {
		global $unit,$retrievalError;
		if($retrievalError == true) {
			echo '404 Error';
		} else {
			echo 'Unit: '.$unit['identnum'].' - ';
		}
	}
	
	function content() {
		global $unit,$retrievalError;
		if($retrievalError == true) {
			echo '<p class="padding15 bg-danger text-danger">You reached a bad page, please go back and try again.</p>';
		} else {
			$division = mysql_fetch_assoc(mysql_query('SELECT name,divid FROM division WHERE id = '.$unit['division'].''));
			?>
				<h1>Unit: <?php echo $unit['identnum'].' - '.$division['divid'].' '.$division['name'].''; ?>
				</h1>
				<div class="row">
					<div class="col-lg-4">
						<h2>Unit Info</h2>
						<h3>Company</h3>
							<?php
								if($unit['company'] == 0) {
									echo '<p class="padding15 bg-danger text-danger">Unit is not assigned to a company.</p>';
								} else {
									$company = mysql_fetch_assoc(mysql_query('SELECT name FROM company WHERE id = '.$unit['company'].''));
									echo '<p>'.$company['name'].'</p>';
								}
							?>
						<h3>Division</h3>
							<?php
								if($unit['division'] == 0) {
									echo '<p class="padding15 bg-danger text-danger">Unit is not assigned to a division.</p>';
								} else {
									$division = mysql_fetch_assoc(mysql_query('SELECT name FROM division WHERE id = '.$unit['division'].''));
									echo '<p>'.$division['divid'].' '.$division['name'].'</p>';
								}
							?>
					</div>
					<div class="col-lg-8">
						<h2>Detailed Information</h2>
						<h3>Equipment <a class="btn btn-default" href="/add/equipment/<?php echo $unit['company']; ?>/<?php echo $unit['division']; ?>/<?php echo $unit['id']; ?>/" role="button">Add</a></h3>
						<?php
							$getEquipment = mysql_query('SELECT * FROM equipment WHERE unit = '.$unit['id'].'');
							if(mysql_num_rows($getEquipment) > 0) {
								while($equipment = mysql_fetch_assoc($getEquipment)) {
									echo '<a title="'.$equipment['make'].' '.$equipment['model'].' '.$equipment['serial'].'" class="attach-button btn btn-default" href="/equipment/'.$equipment['id'].'/" role="button">';
										switch($equipment['type']) {
											case 0: echo '<i class="fa fa-file"></i>';
											break;
											case 1: echo '<i class="fa fa-tachometer"></i>';
											break;
											case 2: echo '<i class="fa fa-bolt"></i>';
											break;
											case 3: echo '<i class="fa fa-empire"></i>';
											break;
											case 4: echo '<i class="fa fa-cube"></i>';
											break;
											case 5: echo '<i class="fa fa-medkit"></i>';
											break;
											case 6: echo '<i class="fa fa-exchange"></i>';
											break;
											case 7: echo '<i class="fa fa-hand-o-left"></i>';
											break;
											case 8: echo '<i class="fa fa-random"></i>';
											break;
										}
									echo $equipment['make'].' '.$equipment['model'];
									if($equipment['serial'] != 'NULL') {
										echo $equipment['serial'];
									}
									echo '</a>';
								}
							} else {
								echo '<p class="padding15 bg-danger text-danger">No equipment has been added to this unit.</p>';
							}
						?>
						<h3>Unit Specific Notes</h3>
						<?php
							if(strlen($unit['notes']) > 3) {
								echo '<p>'.nl2br($unit['notes']).'</p>';
							} else {
								echo '<p class="padding15 bg-danger text-danger">No notes are available for this unit.</p>';
							}
						?>
						<h3>Log History <a class="btn btn-default" href="/add/log/<?php echo $unit['company']; ?>/<?php echo $unit['division']; ?>/<?php echo $unit['id']; ?>/" role="button">Add</a></h3>
						<?php
							$getLogs = mysql_query('SELECT * FROM log WHERE unit = '.$unit['id'].' ORDER BY time DESC LIMIT 20');
							if(mysql_num_rows($getLogs) > 0) {
								echo '<ul>';
								while($log = mysql_fetch_assoc($getLogs)) {
									echo '<li><a href="/log/'.$log['id'].'/">'.date('m/d/y g:i A - ',$log['time']).''.$log['title'].'</a></li>';
								}
								echo '</ul>';
							} else {
								echo '<p class="padding15 bg-danger text-danger">No Recent Logs.</p>';
							}
						?>
						<h3>Attachments <a class="btn btn-default" href="/add/attachment/unit/<?php echo $unit['id']; ?>/" role="button">Add</a></h3>
						<?php
							$getAttachments = mysql_query('SELECT * FROM attachment WHERE infoTypeLink = 3 AND attachedTo = '.$unit['id'].'');
							if(mysql_num_rows($getAttachments) > 0) {
								while($attachment = mysql_fetch_assoc($getAttachments)) {
									echo '<a title="'.$attachment['name'].'" class="attach-button btn btn-default" href="'.$attachment['fileLocation'].'" role="button" target="_blank">';
										switch($attachment['filetype']) {
											case 0: echo '<i class="fa fa-file"></i>';
											break;
											case 1: echo '<i class="fa fa-file-pdf-o"></i>';
											break;
											case 2: echo '<i class="fa fa-file-excel-o"></i>';
											break;
											case 3: echo '<i class="fa fa-file-word-o"></i>';
											break;
											case 4: echo '<i class="fa fa-file-text-o"></i>';
											break;
											case 5: echo '<i class="fa fa-file-image-o"></i>';
											break;
											case 6: echo '<i class="fa fa-file-archive-o"></i>';
											break;
											case 7: echo '<i class="fa fa-file-video-o"></i>';
											break;
											case 8: echo '<i class="fa fa-file-powerpoint-o"></i>';
											break;
											case 9: echo '<i class="fa fa-file-audio-o"></i>';
											break;
										}
									echo $attachment['name'].'</a>';
								}
							} else {
								echo '<p class="padding15 bg-danger text-danger">This unit has no attachments.</p>';
							}
						?>
					</div>
				</div>
			<?
		}
	}