<?php
    require('functions.php');
	config();
    dbconn();
    session_start();
	if(loggedin()) {
		$loggedin = true;
		$user = mysql_fetch_assoc(mysql_query('SELECT email,firstname,lastname FROM user WHERE id = '.$_SESSION['u_'].''));
		goldenPage_retriever();
	} else {
		function title() {
			echo 'Login';
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?php if(function_exists('title')) { title(); echo ' - '; } ?>SFS OfficePAK Mobile</title>
        <base href="http://mobileservice.sfs.website/" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<link rel="stylesheet" href="/css/bootstrap.min.css" />
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        <link rel="stylesheet" href="/css/style.css">
		<script src="/js/jquery-1.11.3.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
		<script src="js/mgmenu_plugins.js"></script><!-- Mega Menu Plugins -->
		<script src="js/mgmenu.js"></script><!-- Mega Menu Script -->
		<script>
		$(document).ready(function($){
			$('#mgmenu1').universalMegaMenu({
				menu_effect: 'hover_slide',
				menu_speed_show: 300,
				menu_speed_hide: 200,
				menu_speed_delay: 200,
				menu_click_outside: true,
				menubar_trigger : false,
				menubar_hide : false,
				menu_responsive: true
			});
		});
		</script>
		<?php if(function_exists('addToHead')) { addToHead(); } ?>
	</head>
	<body onload="load();">
		<section id="header">
			<div class="container">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-8">
					<a href="/"><img src="/images/logo.png" width="463px" height="100px" class="pull-left img-responsive" /></a>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2" style="text-align: right;">
					<?php
						if($loggedin == true) {
							echo '<h3 style="margin: 0;">'.$user['firstname'].' '.$user['lastname'].'</h3>';
							echo '<p><a href="">User Summary</a></p>';
							echo '<p class="small"><a href="">Edit Profile</a> &nbsp; &nbsp; <a href="/logout/">Logout</a></p>';
						}
					?>
				</div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
					<?php
						if($loggedin == true) {
							echo '<img src="'.get_gravatar($user['email']).'" />';
						}
					?>
				</div>
			</div>
		</section>
		<section id="navigation">
			<div class="container">
				<div id="mgmenu1" class="mgmenu_container"><!-- Begin Mega Menu Container -->
					<ul class="mgmenu"><!-- Begin Mega Menu -->
						<li class="mgmenu_button">Mega Menu</li><!-- Button (Mobile Devices) -->
						<li><a href="/"><i class="fa fa-cloud"></i></a></li>
						<li><a href="/contacts/"><i class="fa fa-users"></i>Contacts</a></li>
						<li><a href="/companies/"><i class="fa fa-building"></i>Companies</a></li>
						<li><a href="/units/"><i class="fa fa-truck"></i>Units</a></li>
						<li><a href="/sales/"><i class="fa fa-usd"></i>Sales</a></li>
						<li><a href="/leads/"><i class="fa fa-comments-o"></i>Leads</a></li>
						<li><a href="/documents/"><i class="fa fa-file-text-o"></i>Documents</a></li>
						<li><span><i class="mini_icon ic_documents"></i>Reports</span><!-- Begin Item -->
							<div class="dropdown_container dropdown_fullwidth"><!-- Begin Item Container -->
								<div class="col_2">
									<h4>View Logs by</h4>
									<ul>
										<li><a href="#">Company</a></li>
										<li><a href="#">Division</a></li>
										<li><a href="#">Unit</a></li>
										<li><a href="#">Equipment</a></li>
									</ul>
								</div>
								<div class="col_2">
									<h4>Activity Reports</h4>
								</div>
								<div class="col_2">
									<h4>Equipment Reports</h4>
								</div>
								<div class="col_2">
									<h4>Audit Reports</h4>
								</div>
								<div class="col_4">
									<p class="text_box">Click on the links to the left to generate different reports that will gather details about different topics.</p>
								</div>
							</div><!-- End Item Container -->
						</li><!-- End Item -->
						<li><span><i class="fa fa-wrench"></i>Tools</span><!-- Begin Item -->
							<div class="dropdown_container dropdown_fullwidth"><!-- Begin Item Container -->
								<div class="col_2">
									<h4>View Logs by</h4>
									<ul>
										<li><a href="#">Company</a></li>
										<li><a href="#">Division</a></li>
										<li><a href="#">Unit</a></li>
										<li><a href="#">Equipment</a></li>
									</ul>
								</div>
								<div class="col_2">
									<h4>Activity Reports</h4>
								</div>
								<div class="col_2">
									<h4>Equipment Reports</h4>
								</div>
								<div class="col_2">
									<h4>Audit Reports</h4>
								</div>
								<div class="col_4">
									<p class="text_box">Click on the links to the left to generate different reports that will gather details about different topics.</p>
								</div>
							</div><!-- End Item Container -->
						</li><!-- End Item -->
						<li class="right_item"><span><i class="fa fa-cogs"></i></span><!-- Begin Item -->
							<div class="dropdown_container dropdown_fullwidth"><!-- Begin Item Container -->
								<div class="col_3">
									<h4>Users</h4>
									<ul>
										<li><a href="/users/">Users</a></li>
										<li><a href="#">Permissions</a></li>
									</ul>
								</div>
								<div class="col_2">
									<h4>..</h4>
								</div>
								<div class="col_2">
									<h4>..</h4>
								</div>
								<div class="col_3">
									<h4>Other</h4>
									<ul>
										<li><a href="/bugs-features/">Report Bugs<br />Request Features</a></li>
									</ul>
								</div>
								<div class="col_4">
									<p class="text_box">Use the links to the left to make adjustments to the system.</p>
								</div>
							</div><!-- End Item Container -->
						</li><!-- End Item -->
					</ul><!-- End Mega Menu -->
				</div><!-- End Mega Menu Container -->
			</div>
		</section>
		<section id="content">
			<div class="container">
				<?php
					if($loggedin == true) {
						if(function_exists(content)) {
							content();
						}
					} else {
						require('page_login.php');
					}
				?>
			</div>
		</section>
		<section id="footer">
			<div class="container">
				<div class="col-lg-3">
				<h4>Spray Foam Systems, LLC</h4>
				<p>1502 Airport Rd.<br />Greensboro, GA 30642</p>
				<p><a href="tel:187777374362">1-877-737-4362</a><br /><a href="mailto:sales@sprayfoamsys.com">sales@sprayfoamsys.com</a></p>
				</div>
				<div class="col-lg-3">
				<h4>Technical Contacts</h4>
				<p>Jim Peterson - <a href="tel:7068184053">706-818-4053</a></p>
				<p>Cole Fletcher - <a href="tel:7068180377">706-818-0377</a></p>
				<p>Jeremy Reece - <a href="tel:7702963962">770-296-3962</a></p>
				</div>
				<div class="col-lg-3">
				<h4>Sales Contacts</h4>
				<p>Office - <a href="tel:18777374362">1-877-737-4362</a></p>
				</div>
				<div class="col-lg-3">
				<h4>Online Sales</h4>
				<p><a href="http://sprayfoamsys.com/store/" target="_blank">Spray Foam Systems</a></p>
				</div>
			</div>
		</section>
	</body>
</html>