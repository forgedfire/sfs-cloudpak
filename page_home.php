<?php
	function title() {
		echo 'Home';
	}
	
	function content() {
		?>
			<h1>Welcome</h1>
			<div class="row">
				<div class="col-lg-4">
					<h3>Unit Construction</h3>
					<p class="padding15 bg-danger text-danger">No units under construction.</p>
				</div>
				<div class="col-lg-4">
					<h3>Pending Leads</h3>
					<p class="padding15 bg-danger text-danger">No pending leads.</p>
				</div>
				<div class="col-lg-4">
					<h3>Company Updates</h3>
					<p class="padding15 bg-danger text-danger">No updates.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<h3>Pending Tickets</h3>
					<p class="padding15 bg-danger text-danger">No pending tickets.</p>
				</div>
				<div class="col-lg-4">
					<h3>Pending Repairs</h3>
					<p class="padding15 bg-danger text-danger">No pending repairs.</p>
				</div>
				<div class="col-lg-4">
					<h3>Another Header</h3>
				</div>
			</div>
		<?
	}