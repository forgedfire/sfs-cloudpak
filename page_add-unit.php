<?php
	global $companyid,$divisionid;
	$companyid = $_GET['companyid'];
	$divisionid = $_GET['divisionid'];

	function title() {
		echo 'Add New Division';
	}
	
	function content() {
		global $companyid,$divisionid;
		?>
			<h1>Add New Unit <a class="btn btn-default" href="/division/<?php echo $divisionid; ?>/" role="button">Back to division</a></h1>
			<p>Fill out the form below and submit to add a new unit.</p>
			<?php
				if(isset($_POST['addUnit'])) {
						$query = 'INSERT INTO unit (id,status,company,division,type,identnum,lead,leadcell) VALUES (0,1,'.$companyid.','.$divisionid.','.$_POST['type'].',"'.$_POST['identnum'].'","'.$_POST['lead'].'","'.$_POST['leadcell'].'")';
					if(mysql_query($query)) {
						echo '<p class="padding15 bg-success text-success">You have sucessfully added a new unit called <strong>'.$_POST['identnum'].'</strong>"</p>';
					} else {
						echo '<p class="padding15 bg-danger text-danger">The unit wasn\'t added, something went wrong, talk to IT Brett.<br /><br />ERROR: '.mysql_error().'<br /><br />'.$query.'</p>';
					}
				}
			?>
			<form action="" method="POST">
				<div class="form-group">
					<label for="type">Unit Type</label>
					<select name="type" id="type" class="form-control">
						<option>Please Select</option>
						<option value="1">Rig</option>
						<option value="2">Portable</option>
						<option value="3">Standalone</option>
					</select>
				</div>
				<div class="form-group">
					<label for="identnum">Unit ID</label>
					<input type="text" class="form-control" name="identnum" id="identnum" placeholder="Unit ID (ex. 770 OR AFB0378">
				</div>
				<div class="form-group">
					<label for="lead">Lead</label>
					<input type="text" class="form-control" name="lead" id="lead" placeholder="Lead">
				</div>
				<div class="form-group">
					<label for="leadcell">Lead Cell</label>
					<input type="text" class="form-control" name="leadcell"  id="leadcell" placeholder="Lead Cell">
				</div>
				<button type="submit" name="addUnit" class="btn btn-default">Submit</button>
			</form>
		<?
	}