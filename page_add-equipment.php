<?php
	global $companyid,$divisionid,$unitid;
	$companyid = $_GET['companyid'];
	$divisionid = $_GET['divisionid'];
	$unitid = $_GET['unitid'];

	function title() {
		echo 'Add New Equipment';
	}
	
	function content() {
		global $companyid,$divisionid,$unitid;
		?>
			<h1>Add New Equipment <a class="btn btn-default" href="/unit/<?php echo $unitid; ?>/" role="button">Back to unit</a></h1>
			<p>Fill out the form below and submit to add new equipment.</p>
			<?php
				global $companyid,$divisionid,$unitid;
				if(isset($_POST['addEquipment'])) {
					if(strlen($_POST['serial']) > 3) {
						$serial = $_POST['serial'];
					} else {
						$serial = 'NULL';
					}
					if(mysql_query('
						INSERT INTO equipment (
							id,
							status,
							company,
							division,
							unit,
							type,
							make,
							model,
							serial,
							functioning,
							notes
						) VALUES (
							0,
							1,
							'.$companyid.',
							'.$divisionid.',
							'.$unitid.',
							'.$_POST['type'].',
							"'.$_POST['make'].'",
							"'.$_POST['model'].'",
							"'.$serial.'",
							1,
							"'.$_POST['notes'].'"
						)')) {
						echo '<p class="padding15 bg-success text-success">You have sucessfully added new equipment</p>';
					} else {
						echo '<p class="padding15 bg-danger text-danger">The equipment wasn\'t added, something went wrong, talk to IT Brett.<Br /><br />'.mysql_error().'</p>';
					}
				}
			?>
			<form action="" method="POST">
				<div class="form-group">
					<label for="type">Equipment Type</label>
					<select class="form-control" name="type" id="type">
						<option>Please Select</option>
						<option value="1">Proportioner</option>
						<option value="2">Generator</option>
						<option value="3">Air Compressor</option>
						<option value="4">Air Dryer</option>
						<option value="5">Supplied Air</option>
						<option value="6">Transfer Pump</option>
						<option value="7">Spray Gun</option>
						<option value="8">Hose</option>
					</select>
				</div>
				<div class="form-group">
					<label for="make">Make</label>
					<input type="text" class="form-control" name="make" id="make" placeholder="Make">
				</div>
				<div class="form-group">
					<label for="model">Model Number</label>
					<input type="text" class="form-control" name="model" id="model" placeholder="Model">
				</div>
				<div class="form-group">
					<label for="serial">Serial #</label>
					<input type="text" class="form-control" name="serial"  id="serial" placeholder="Serial #">
				</div>
				<div class="form-group">
					<label for="notes">Notes</label>
					<textarea class="form-control" rows="6" name="notes"  id="notes" placeholder="Add Notes"></textarea>
				</div>
				<button type="submit" name="addEquipment" class="btn btn-default">Submit</button>
			</form>
		<?
	}