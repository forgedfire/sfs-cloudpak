<?php
	$getCompany = mysql_query('SELECT * FROM company WHERE id = '.$_GET['id'].'');
	if(mysql_num_rows($getCompany) > 0) {
		global $company;
		$company = mysql_fetch_assoc($getCompany);
	} else {
		global $retrievalError;
		$retrievalError = true;
	}

	function title() {
		global $company,$retrievalError;
		if($retrievalError == true) {
			echo '404 Error';
		} else {
			echo 'Company: '.$company['name'].'';
		}
	}
	
	function addToHead() {
		global $company;
		?>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDV7723qLTXyzEiC3d-UQxNWKV7w5R0oP8" type="text/javascript"></script>
		<script type="text/javascript">
			//<![CDATA[
				var customIcons = {
					customer: {
						icon: 'http://labs.google.com/ridefinder/images/mm_20_blue.png'
					},
					bar: {
						icon: 'http://labs.google.com/ridefinder/images/mm_20_red.png'
					}
				};
				function load() {
					var map = new google.maps.Map(document.getElementById("map"), {
						center: new google.maps.LatLng(40.213459, -95.646860),
						zoom: 4,
						mapTypeId: 'roadmap',
						scrollwheel: false
					});
					var infoWindow = new google.maps.InfoWindow;

				// Change this depending on the name of your PHP file
				downloadUrl("/map-xmls/division-map.php?companyid=<?php echo $company['id'] ?>", function(data) {
					var xml = data.responseXML;
					var markers = xml.documentElement.getElementsByTagName("marker");
						for (var i = 0; i < markers.length; i++) {
							var name = markers[i].getAttribute("name");
							var address = markers[i].getAttribute("address");
							var type = markers[i].getAttribute("type");
							var point = new google.maps.LatLng(
								parseFloat(markers[i].getAttribute("lat")),
								parseFloat(markers[i].getAttribute("lng")));
							var html = "<b>" + name + "</b> <br/>" + address;
							var icon = customIcons[type] || {};
							var marker = new google.maps.Marker({
								map: map,
								position: point,
								icon: icon.icon
							});
							bindInfoWindow(marker, map, infoWindow, html);
						}
					});
				}
				function bindInfoWindow(marker, map, infoWindow, html) {
					google.maps.event.addListener(marker, 'click', function() {
						infoWindow.setContent(html);
						infoWindow.open(map, marker);
					});
				}
				function downloadUrl(url, callback) {
					var request = window.ActiveXObject ?
					new ActiveXObject('Microsoft.XMLHTTP') :
					new XMLHttpRequest;
				request.onreadystatechange = function() {
					if (request.readyState == 4) {
						request.onreadystatechange = doNothing;
						callback(request, request.status);
					}
				};
				request.open('GET', url, true);
				request.send(null);
			}
		function doNothing() {}
		//]]>
		</script>
		<?
	}
	
	function content() {
		global $company,$retrievalError;
		if($retrievalError == true) {
			echo '<p class="padding15 bg-danger text-danger">You reached a bad page, please go back and try again.</p>';
		} else {
			?>
				<h1>Company: <?php echo $company['name']; ?> <a class="btn btn-default" href="/companies/" role="button">Back to companies</a></h1>
				<p>We manage <strong><? echo mysql_num_rows(mysql_query('SELECT * FROM unit WHERE company = '.$company['id'].'')) ?></strong> units across <strong><? echo mysql_num_rows(mysql_query('SELECT * FROM division WHERE company = '.$company['id'].'')) ?></strong> divisions for <?php echo $company['name']; ?>. Please view the information below to find out the information you need for this company.</p>
				<div class="row">
					<div class="col-lg-4">
						<h2>Company Info</h2>
						<h3>Address</h3>
						<p><a href="http://maps.google.com/maps?q=<?php echo str_replace(" ",'+',$company['address1']).'+'.$company['city'].'+'.$company['state'].'+'.$company['zip']; ?>" target="_blank"><?php echo $company['address1'].'<br />'; if(strlen($company['address2']) > 3) { echo $company['address2'].'<br />'; } echo $company['city'].', '; echo $company['state'].' '; echo $company['zip'] ?></a></p>
					</div>
					<div class="col-lg-4">
						<h2>Divisions <a class="btn btn-default" href="/add/division/<?php echo $company['id']; ?>/" role="button">Add</a></h2>
						<?php
							$getDivisions = mysql_query('SELECT * FROM division WHERE company = '.$company['id'].' AND status = 1 ORDER BY divid ASC');
							if(mysql_num_rows($getDivisions) > 0) {
								echo '<select style="width: 100%;" class="input-lg" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);"><option>Select a division</option>';
								while($division = mysql_fetch_assoc($getDivisions)) {
									echo '<option value="/division/'.$division['id'].'/">'.$division['divid'].' - '.$division['name'].'</option>';
								}
								echo '</select>';
							} else {
								echo '<p class="padding15 bg-danger text-danger">No Associated Divisions.</p>';
							}
						?>
					</div>
					<div class="col-lg-4">
						<h2>Recent Logs <a class="btn btn-default" href="/add/log/<?php echo $company['id']; ?>/" role="button">Add</a></h2>
						<?php
							$getLogs = mysql_query('SELECT * FROM log WHERE company = '.$company['id'].' ORDER BY time DESC LIMIT 20');
							if(mysql_num_rows($getLogs) > 0) {
								echo '<ul>';
								while($log = mysql_fetch_assoc($getLogs)) {
									echo '<li><a href="/log/'.$log['id'].'/">'.date('m/d/y g:i A - ',$log['time']).''.$log['title'].'</a></li>';
								}
								echo '</ul>';
							} else {
								echo '<p class="padding15 bg-danger text-danger">No Recent Logs.</p>';
							}
						?>
					</div>
				</div>
                <div class="row">
                	<div class="col-lg-12">
                    	<h2>Division Map</h2>
                    	<div id="map" style="width: 100%; height: 650px; border: 1px solid #000000;"></div>
                    </div>
                </div>
			<?
		}
	}