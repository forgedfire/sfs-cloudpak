<?php
	global $companyid;
	$companyid = $_GET['companyid'];

	function title() {
		echo 'Add New Division';
	}
	
	function content() {
		global $companyid;
		?>
			<h1>Add New Division <a class="btn btn-default" href="/company/<?php echo $companyid; ?>/" role="button">Back to company</a></h1>
			<p>Fill out the form below and submit to add a new division.</p>
			<?php
				if(isset($_POST['addDivision'])) {
					if(mysql_query('INSERT INTO division (id,status,company,divid,name,address1,address2,city,state,zip,regionallead,rloffice,rlcell,rlemail,divman,dmoffice,dmcell,dmemail,proman,pmoffice,pmcell,pmemail,foreman,fmcell) VALUES (0,1,'.$companyid.',"'.$_POST['divid'].'","'.$_POST['name'].'","'.$_POST['address1'].'","'.$_POST['address2'].'","'.$_POST['city'].'","'.$_POST['state'].'","'.$_POST['zipcode'].'","'.$_POST['regionallead'].'","'.$_POST['rloffice'].'","'.$_POST['rlcell'].'","'.$_POST['rlemail'].'","'.$_POST['divman'].'","'.$_POST['dmoffice'].'","'.$_POST['dmcell'].'","'.$_POST['dmemail'].'","'.$_POST['proman'].'","'.$_POST['pmoffice'].'","'.$_POST['pmcell'].'","'.$_POST['pmemail'].'","'.$_POST['foreman'].'","'.$_POST['fmcell'].'")')) {
						echo '<p class="padding15 bg-success text-success">You have sucessfully added <strong>'.$_POST['name'].'</strong>"</p>';
					} else {
						echo '<p class="padding15 bg-danger text-danger">The division wasn\'t added, something went wrong, talk to IT Brett.</p>';
					}
				}
			?>
			<form action="" method="POST">
				<div class="form-group">
					<label for="name">Division Name</label>
					<input type="text" class="form-control" name="name" id="name" placeholder="Division Name">
				</div>
				<div class="form-group">
					<label for="divid">Division ID</label>
					<input type="text" class="form-control" name="divid" id="divid" placeholder="Division ID (ex. 770 OR AFB0378)">
				</div>
				<div class="form-group">
					<label for="address1">Address 1</label>
					<input type="text" class="form-control" name="address1"  id="address1" placeholder="Address">
				</div>
				<div class="form-group">
					<label for="address2">Address 2</label>
					<input type="text" class="form-control" name="address2"  id="address2" placeholder="Address cont.">
				</div>
				<div class="form-group">
					<label for="city">City</label>
					<input type="text" class="form-control" name="city"  id="city" placeholder="City">
				</div>
				<div class="form-group">
					<label for="state">State</label>
					<input type="text" class="form-control" name="state"  id="state" placeholder="State">
				</div>
				<div class="form-group">
					<label for="zipcode">Zip Code</label>
					<input type="text" class="form-control" name="zipcode"  id="zipcode" placeholder="Zip Code">
				</div>
				<div class="form-group">
					<label for="regionallead">Regional Lead</label>
					<input type="text" class="form-control" name="regionallead"  id="regionallead" placeholder="Regional Lead">
				</div>
				<div class="form-group">
					<label for="rloffice">Regional Lead Office</label>
					<input type="text" class="form-control" name="rloffice"  id="rloffice" placeholder="Regional Lead Office">
				</div>
				<div class="form-group">
					<label for="rlcell">Regional Lead Cell</label>
					<input type="text" class="form-control" name="rlcell"  id="rlcell" placeholder="Regional Lead Cell">
				</div>
				<div class="form-group">
					<label for="rlemail">Regional Lead Email</label>
					<input type="text" class="form-control" name="rlemail"  id="rlemail" placeholder="Regional Lead Email">
				</div>
				<div class="form-group">
					<label for="divman">Division Manager</label>
					<input type="text" class="form-control" name="zipcode"  id="divman" placeholder="Division Manager">
				</div>
				<div class="form-group">
					<label for="dmoffice">Division Manager Office</label>
					<input type="text" class="form-control" name="dmoffice"  id="dmoffice" placeholder="Division Manager Office">
				</div>
				<div class="form-group">
					<label for="dmcell">Division Manager Cell</label>
					<input type="text" class="form-control" name="dmcell"  id="dmcell" placeholder="Division Manager Cell">
				</div>
				<div class="form-group">
					<label for="dmemail">Division Manager Email</label>
					<input type="text" class="form-control" name="dmemail"  id="dmemail" placeholder="Division Manager Email">
				</div>
				<div class="form-group">
					<label for="proman">Production Manager</label>
					<input type="text" class="form-control" name="proman"  id="proman" placeholder="Production Manager">
				</div>
				<div class="form-group">
					<label for="pmoffice">Production Manager Office</label>
					<input type="text" class="form-control" name="pmoffice"  id="pmoffice" placeholder="Production Manager Office">
				</div>
				<div class="form-group">
					<label for="pmcell">Productuion Manager Cell</label>
					<input type="text" class="form-control" name="pmcell"  id="pmcell" placeholder="Productuion Manager Cell">
				</div>
				<div class="form-group">
					<label for="pmemail">Production Manager Email</label>
					<input type="text" class="form-control" name="pmemail"  id="pmemail" placeholder="Production Manager Email">
				</div>
				<div class="form-group">
					<label for="foreman">Foreman</label>
					<input type="text" class="form-control" name="foreman"  id="foreman" placeholder="Foreman">
				</div>
				<div class="form-group">
					<label for="fmcell">Foreman Cell</label>
					<input type="text" class="form-control" name="fmcell"  id="fmcell" placeholder="Foreman Cell">
				</div>
				<button type="submit" name="addDivision" class="btn btn-default">Submit</button>
			</form>
		<?
	}