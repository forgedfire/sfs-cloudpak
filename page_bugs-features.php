<?php
	function title() {
		echo 'Report Bugs / Request Features';
	}
	
	function content() {
		?>
			<h1>Bug Reports and Feature Requests</h1>
			<?php
				if(isset($_POST['addReport'])) {
					if(mysql_query('INSERT INTO bugs_features (
						id,
						status,
						type,
						bf_type,
						title,
						notes
					) VALUES (
						0,
						1,
						'.$_POST['type'].',
						'.$_POST['bf_type'].',
						"'.$_POST['title'].'",
						"'.$_POST['notes'].'"
					)')) {
						echo '<p class="padding15 bg-success text-success">You have sucessfully added a new report/request</p>';
					} else {
						echo '<p class="padding15 bg-danger text-danger">Apparently your report/request sucks, because the system rejected it.<br /><br />ERROR: '.mysql_error().'<br /><br />'.$query.'</p>';
					}
				}
			?>
			<p>This is a list of bug reports and feature requests. It allows IT Brett to know what you want, and what he screwed up when programming the intranet. Don't go talk to him about it, just add them here. When he has a moment, he will get around to handling them.</p>
			<h2>Reports/Requests</h2>
			
				<?php
					$get_bugsFeatures = mysql_query('SELECT * FROM bugs_features WHERE status = 1');
					if(mysql_num_rows($get_bugsFeatures) > 0) {
						echo '<div class="table-responsive"><table class="table table-striped">';
							echo '<tr>';
								echo '<th>#</th>';
								echo '<th>Type</th>';
								echo '<th>Title</th>';
								echo '<th>Notes</th>';
							echo '</tr>';
						while($request = mysql_fetch_assoc($get_bugsFeatures)) {
							echo '<tr>';
							echo '<td>'.$request['id'].'</td>';
							echo '<td>';
								switch($request['type']) {
									case 1: echo 'Bug Report';
										break;
									case 2: echo 'New Feature';
										break;
								}
							echo '</td>';
							echo '<td>'.$request['title'].'</td>';
							echo '<td>'.$request['notes'].'</td>';
							echo '</tr>';
						}
						echo '</table></div>';
					} else {
						echo '<p class="padding15 bg-danger text-danger">There are no pending requests/reports.</p>';
					}
				?>
			<h2>Make a Request</h2>
			<form action="" method="POST">
				<div class="form-group">
					<label for="type">Request Type</label>
					<select name="type" id="type" class="form-control">
						<option>Please Select</option>
						<option value="1">Bug Report</option>
						<option value="2">Feature Request</option>
					</select>
				</div>
				<div class="form-group">
					<label for="bf_type">Bug/Feature Type</label>
					<select name="bf_type" id="bf_type" class="form-control">
						<option>Please Select</option>
						<option value="1">New feature</option>
						<option value="2">Causing minor error</option>
						<option value="3">Functionality Problem</option>
						<option value="4">Mobile Browser Problem</option>
						<option value="5">Critical Function Problem</option>
						<option value="6">Just plain weird</option>
						<option value="7">Other</option>
					</select>
				</div>
				<div class="form-group">
					<label for="title">Title</label>
					<input type="text" class="form-control" name="title" id="title" placeholder="Title">
				</div>
				<div class="form-group">
					<label for="notes">Notes</label>
					<textarea class="form-control" rows="6" name="notes"  id="notes" placeholder="Add Notes"></textarea>
				</div>
				<button type="submit" name="addReport" class="btn btn-default">Submit</button>
			</form>
		<?
	}