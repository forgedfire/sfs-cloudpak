<?php
	function title() {
		echo 'Companies';
	}
	
	function addToHead() {
		?>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDV7723qLTXyzEiC3d-UQxNWKV7w5R0oP8" type="text/javascript"></script>
		<script type="text/javascript">
			//<![CDATA[
				var customIcons = {
					customer: {
						icon: 'http://labs.google.com/ridefinder/images/mm_20_blue.png'
					},
					bar: {
						icon: 'http://labs.google.com/ridefinder/images/mm_20_red.png'
					}
				};
				function load() {
					var map = new google.maps.Map(document.getElementById("map"), {
						center: new google.maps.LatLng(40.213459, -95.646860),
						zoom: 4,
						mapTypeId: 'roadmap',
						scrollwheel: false
					});
					var infoWindow = new google.maps.InfoWindow;

				// Change this depending on the name of your PHP file
				downloadUrl("/map-xmls/companies-map.xml", function(data) {
					var xml = data.responseXML;
					var markers = xml.documentElement.getElementsByTagName("marker");
						for (var i = 0; i < markers.length; i++) {
							var name = markers[i].getAttribute("name");
							var address = markers[i].getAttribute("address");
							var type = markers[i].getAttribute("type");
							var point = new google.maps.LatLng(
								parseFloat(markers[i].getAttribute("lat")),
								parseFloat(markers[i].getAttribute("lng")));
							var html = "<b>" + name + "</b> <br/>" + address;
							var icon = customIcons[type] || {};
							var marker = new google.maps.Marker({
								map: map,
								position: point,
								icon: icon.icon
							});
							bindInfoWindow(marker, map, infoWindow, html);
						}
					});
				}
				function bindInfoWindow(marker, map, infoWindow, html) {
					google.maps.event.addListener(marker, 'click', function() {
						infoWindow.setContent(html);
						infoWindow.open(map, marker);
					});
				}
				function downloadUrl(url, callback) {
					var request = window.ActiveXObject ?
					new ActiveXObject('Microsoft.XMLHTTP') :
					new XMLHttpRequest;
				request.onreadystatechange = function() {
					if (request.readyState == 4) {
						request.onreadystatechange = doNothing;
						callback(request, request.status);
					}
				};
				request.open('GET', url, true);
				request.send(null);
			}
		function doNothing() {}
		//]]>
		</script>
		<?
	}
	
	function content() {
		?>
			<h1>Companies <a class="btn btn-default" href="/add/company/" role="button">Add</a></h1>
			<p>Select a company from the list below.</p>
            <div class="row">
				<?php
					$getCompanies = mysql_query('SELECT * FROM company WHERE status = 1');
					if(mysql_num_rows($getCompanies) > 0) {
						while($company = mysql_fetch_assoc($getCompanies)) {
							echo '<div class="col-lg-3"><a style="width: 100%;" class="btn btn-default" href="/company/'.$company['id'].'/" role="button">'.$company['name'].'</a></div>';
						}
					} else {
						echo '<div class="col-lg-12"><p class="padding15 bg-danger text-danger">Sorry, there are currently no active companies listed.</p></div>';
					}
				?>
            </div>
            <div class="row">
            	<div class="col-lg-12">
                	<h2>Company Map</h2>
            		<div id="map" style="width: 100%; height: 650px; border: 1px solid #000000;"></div>
                </div>
            </div>
		<?
	}