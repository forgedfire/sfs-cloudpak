<?php
	$getDivision = mysql_query('SELECT * FROM division WHERE id = '.$_GET['id'].'');
	if(mysql_num_rows($getDivision) > 0) {
		global $division;
		$division = mysql_fetch_assoc($getDivision);
	} else {
		global $retrievalError;
		$retrievalError = true;
	}

	function title() {
		global $division,$retrievalError;
		if($retrievalError == true) {
			echo '404 Error';
		} else {
			echo 'Division: '.$division['name'].'';
		}
	}
	
	function content() {
		global $division,$retrievalError;
		if($retrievalError == true) {
			echo '<p class="padding15 bg-danger text-danger">You reached a bad page, please go back and try again.</p>';
		} else {
			?>
				<h1>Division: <?php echo $division['divid'].' - '.$division['name']; ?> <a class="btn btn-default" href="/company/<?php echo $division['company']; ?>/" role="button">Back to company</a></h1>
				<p>We manage <strong><? echo mysql_num_rows(mysql_query('SELECT * FROM unit WHERE division = '.$division['id'].'')) ?></strong> units for <strong><?php echo $division['divid'].' - '.$division['name']; ?></strong>. Please view the information below to find out the information you need for this division.</p>
				<div class="row">
					<div class="col-lg-4">
						<h2>Division Info</h2>
						<h3>Address</h3>
						<p><a href="http://maps.google.com/maps?q=<?php echo str_replace(" ",'+',$division['address1']).'+'.$division['city'].'+'.$division['state'].'+'.$division['zip']; ?>" target="_blank"><?php echo $division['address1'].'<br />'; if(strlen($division['address2']) > 3) { echo $division['address2'].'<br />'; } echo $division['city'].', '; echo $division['state'].' '; echo $division['zip'] ?></a></p>
						<h3>Regional Lead</h3>
						<p>Name: <?php echo $division['regionallead']; ?><br />Office: <a href="tel:<?php echo $division['rloffice']; ?>"><?php echo $division['rloffice']; ?></a><br />Cell: <a href="tel:<?php echo $division['rlcell']; ?>"><?php echo $division['rlcell']; ?></a><br />Email: <a href="mailto:<?php echo $division['rlemail']; ?>"><?php echo $division['rlemail']; ?></a></p>
						<h3>Division Manager</h3>
						<p>Name: <?php echo $division['divman']; ?><br />Office: <a href="tel:<?php echo $division['dmoffice']; ?>"><?php echo $division['dmoffice']; ?></a><br />Cell: <a href="tel:<?php echo $division['dmcell']; ?>"><?php echo $division['dmcell']; ?></a><br />Email: <a href="mailto:<?php echo $division['dmemail']; ?>"><?php echo $division['dmemail']; ?></a></p>
						<h3>Production Manager</h3>
						<p>Name: <?php echo $division['proman']; ?><br />Office: <a href="tel:<?php echo $division['pmoffice']; ?>"><?php echo $division['pmoffice']; ?></a><br />Cell: <a href="tel:<?php echo $division['pmcell']; ?>"><?php echo $division['pmcell']; ?></a><br />Email: <a href="mailto:<?php echo $division['pmemail']; ?><?php echo $division['pmemail']; ?></a></p>
						<h3>Foreman</h3>
						<p>Name: <?php echo $division['foreman']; ?><br />Cell: <a href="tel:<?php echo $division['fmcell']; ?>"><?php echo $division['fmcell']; ?></a></p>
					</div>
					<div class="col-lg-4">
						<h2>Units <a class="btn btn-default" href="/add/unit/<?php echo $division['company']; ?>/<?php echo $division['id']; ?>/" role="button">Add</a></h2>
						<?php
							$getUnits = mysql_query('SELECT * FROM unit WHERE division = '.$division['id'].' AND status = 1 ORDER BY id ASC');
							if(mysql_num_rows($getUnits) > 0) {
								echo '<select style="width: 100%;" class="input-lg" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);"><option>Select a unit</option>';
								while($unit = mysql_fetch_assoc($getUnits)) {
									echo '<option value="/unit/'.$unit['id'].'/">'.$unit['identnum'].'</option>';
								}
								echo '</select>';
							} else {
								echo '<p class="padding15 bg-danger text-danger">No Associated units.</p>';
							}
						?>
					</div>
					<div class="col-lg-4">
						<h2>Recent Logs <a class="btn btn-default" href="/add/log/<?php echo $division['company']; ?>/<?php echo $division['id']; ?>/" role="button">Add</a></h2>
						<?php
							$getLogs = mysql_query('SELECT * FROM log WHERE division = '.$division['id'].' ORDER BY time DESC LIMIT 20');
							if(mysql_num_rows($getLogs) > 0) {
								echo '<ul>';
								while($log = mysql_fetch_assoc($getLogs)) {
									echo '<li><a href="/log/'.$log['id'].'/">'.date('m/d/y g:i A - ',$log['time']).''.$log['title'].'</a></li>';
								}
								echo '</ul>';
							} else {
								echo '<p class="padding15 bg-danger text-danger">No Recent Logs.</p>';
							}
						?>
					</div>
				</div>
			<?
		}
	}