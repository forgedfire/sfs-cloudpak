<?php
	header("Content-type: text/xml");
	require('../functions.php');
	dbconn();
	function parseToXML($htmlStr) {
		$xmlStr=str_replace('<','&lt;',$htmlStr);
		$xmlStr=str_replace('>','&gt;',$xmlStr);
		$xmlStr=str_replace('"','&quot;',$xmlStr);
		$xmlStr=str_replace("'",'&#39;',$xmlStr);
		$xmlStr=str_replace("&",'&amp;',$xmlStr);
		return $xmlStr;
	}
	
	$getMarkers = mysql_query('SELECT * FROM division WHERE company = '.$_GET['companyid'].' AND status = 1 AND longitude != "" AND latitude != ""');
	if(mysql_num_rows($getMarkers) > 0) {
		echo '<markers>';
		while($marker = mysql_fetch_assoc($getMarkers)) {
			echo '<marker ';
			echo 'name="' . parseToXML($marker['name']) . '" ';
			echo 'address="'.parseToXML($marker['address1'].' '.$marker['city'].', '.$marker['state']).' '.$marker['zip'].'" ';
			echo 'lat="' . $marker['latitude'] . '" ';
			echo 'lng="' . $marker['longitude'] . '" ';
			echo 'type="customer" ';
			echo '/>';
		}
		echo '</markers>';
	}
	dbclose();