<?php
	function title() {
		echo 'Leads';
	}
	
	function content() {
		?>
			<h1>Leads <a class="btn btn-default" href="/add/lead/" role="button">Add</a></h1>
			<p>View incoming/current leads, assign them, and update leads with new information.</p>
			<h2>View Assigned Leads</h2>
			<div class="row">
				<div class="col-lg-6">
					<h3>Active Leads</h3>
					<p class="padding15 bg-danger text-danger">No pending leads.</p>
				</div>
				<div class="col-lg-6">
					<h3>Non-pursued leads</h3>
					<p class="padding15 bg-danger text-danger">No pending leads.</p>
				</div>
			</div>
			<h2>Unassigned Leads</h2>
			<div class="row">
				<div class="col-lg-6">
					<h3>Website Leads</h3>
					<p class="padding15 bg-danger text-danger">No pending tickets.</p>
				</div>
				<div class="col-lg-6">
					<h3>Phone Leads</h3>
					<p class="padding15 bg-danger text-danger">No pending repairs.</p>
				</div>
			</div>
		<?
	}