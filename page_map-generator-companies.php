<?php
	function title() {
		echo 'Generate Google Map for Companies';
	}
	
	function content() {
		?>
			<h1>Generate Google Map for Companies</h1>
			<?php
				if(isset($_POST['generateMap'])) {
					function parseToXML($htmlStr) {
						$xmlStr=str_replace('<','&lt;',$htmlStr);
						$xmlStr=str_replace('>','&gt;',$xmlStr);
						$xmlStr=str_replace('"','&quot;',$xmlStr);
						$xmlStr=str_replace("'",'&#39;',$xmlStr);
						$xmlStr=str_replace("&",'&amp;',$xmlStr);
						return $xmlStr;
					}
					
					$getMarkers = mysql_query('SELECT * FROM company WHERE status = 1 AND longitude != "" AND latitude != ""');
					if(mysql_num_rows($getMarkers) > 0) {
						$xmlGenerate = '<markers>';
						while($marker = mysql_fetch_assoc($getMarkers)) {
							$xmlGenerate.= '<marker ';
							$xmlGenerate.= 'name="' . parseToXML($marker['name']) . '" ';
							$xmlGenerate.= 'address="'.parseToXML($marker['address1'].' '.$marker['city'].', '.$marker['state']).' '.$marker['zip'].'" ';
							$xmlGenerate.= 'lat="' . $marker['latitude'] . '" ';
							$xmlGenerate.= 'lng="' . $marker['longitude'] . '" ';
							$xmlGenerate.= 'type="customer" ';
							$xmlGenerate.= '/>';
						}
						$xmlGenerate.= '</markers>';
						if(file_put_contents($_SERVER["DOCUMENT_ROOT"].'/map-xmls/companies-map.xml', $xmlGenerate)) {
							echo '<p class="padding15 bg-success text-success">The generator has completed and new maps are available.</p>';
						} else {
							echo '<p class="padding15 bg-danger text-danger">There was a problem, and the data was not written to file. Talk to IT Brett</p>';
						}
					} else {
						echo '<p class="padding15 bg-danger text-danger">There are no locations with Longitude, and Latitudes set.</p>';
					}
				}
			?>
            <p>Click the button below to generate an updated Google Map for the company page.</p>
            <form method="POST"><button type="submit" name="generateMap" class="btn btn-default">Update Google Map</button></form>
		<?
	}