<?php
	function title() {
		echo 'Add New Company';
	}
	
	function content() {
		?>
			<h1>Add New Company <a class="btn btn-default" href="/companies/" role="button">Back to companies</a></h1>
			<p>Fill out the form below and submit to add a new company.</p>
			<?php
				if(isset($_POST['addCompany'])) {
					if(mysql_query('INSERT INTO company (id,status,name,address1,address2,city,state,zip) VALUES (0,1,"'.$_POST['name'].'","'.$_POST['address1'].'","'.$_POST['address2'].'","'.$_POST['city'].'","'.$_POST['state'].'","'.$_POST['zipcode'].'")')) {
						echo '<p class="padding15 bg-success text-success">You have sucessfully added <strong>'.$_POST['name'].'</strong>"</p>';
					} else {
						echo '<p class="padding15 bg-danger text-danger">The company wasn\'t added, something went wrong, talk to IT Brett.</p>';
					}
				}
			?>
			<form action="" method="POST">
				<div class="form-group">
					<label for="name">Company Name</label>
					<input type="text" class="form-control" name="name" id="name" placeholder="Company Name">
				</div>
				<div class="form-group">
					<label for="address1">Address 1</label>
					<input type="text" class="form-control" name="address1"  id="address1" placeholder="Address">
				</div>
				<div class="form-group">
					<label for="address2">Address 2</label>
					<input type="text" class="form-control" name="address2"  id="address2" placeholder="Address cont.">
				</div>
				<div class="form-group">
					<label for="city">City</label>
					<input type="text" class="form-control" name="city"  id="city" placeholder="City">
				</div>
				<div class="form-group">
					<label for="state">State</label>
					<input type="text" class="form-control" name="state"  id="state" placeholder="State">
				</div>
				<div class="form-group">
					<label for="zipcode">Zip Code</label>
					<input type="text" class="form-control" name="zipcode"  id="zipcode" placeholder="Zip Code">
				</div>
				<button type="submit" name="addCompany" class="btn btn-default">Submit</button>
			</form>
		<?
	}