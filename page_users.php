<?php
	function title() {
		echo 'Manage Users';
	}
	
	function content() {
		?>
			<h1>Manage Users <a class="btn btn-default" href="/add/user/" role="button">Add</a></h1>
			<div class="row">
				<?php
					$getUsers = mysql_query('SELECT * FROM user WHERE status = 1');
					if(mysql_num_rows($getUsers) > 0) {
						while($user = mysql_fetch_assoc($getUsers)) {
							echo '<div class="col-lg-3"><a style="width: 100%;" class="btn btn-default" href="/user/'.$user['id'].'/" role="button">'.$user['lastname'].', '.$user['firstname'].'</a></div>';
						}
					} else {
						echo '<div class="col-lg-12"><p class="padding15 bg-danger text-danger">Sorry, there are currently no active users listed.</p></div>';
					}
				?>
            </div>
		<?
	}