<?php
	if(isset($_GET['companyid'])) {
		global $companyid;
		$companyid = $_GET['companyid'];
	}
	
	if(isset($_GET['divisionid'])) {
		global $divisionid;
		$divisionid = $_GET['divisionid'];
	}
	
	if(isset($_GET['unitid'])) {
		global $unitid;
		$unitid = $_GET['unitid'];
	}

	function title() {
		echo 'Add New Log';
	}
	
	function addToHead() {
		?>
			<link href="/css/bootstrap-datetimepicker.css" rel="stylesheet">
			<script src="/js/moment-with-locales.js"></script>
			<script src="/js/bootstrap-datetimepicker.js"></script>
		<?
	}
	
	function content() {
		?>
			<h1>Add New Log</h1>
			<p>Fill out the form below and submit to add a new log.</p>
			<?php
				global $companyid;
				if(isset($_POST['addLog'])) {
					if(mysql_query('
						INSERT INTO log (
							id,
							time,
							company,
							division,
							unit,
							equipment,
							title,
							contact,
							type,
							quicknote,
							notes
						) VALUES (
							0,
							"'.strtotime($_POST['datetime']).'",
							"'.$_POST['company'].'",
							"'.$_POST['division'].'",
							"'.$_POST['unit'].'",
							"'.$_POST['equipment'].'",
							"'.$_POST['title'].'",
							"'.$_POST['contact'].'",
							"'.$_POST['type'].'",
							"'.$_POST['quicknote'].'",
							"'.$_POST['notes'].'"
						)
					')) {
						echo '<p class="padding15 bg-success text-success">You have sucessfully added a new log.</p>';
					} else {
						echo '<p class="padding15 bg-danger text-danger">The log wasn\'t added, something went wrong, talk to IT Brett.<br />'.mysql_error().'</p>';
					}
				}
			?>
			<form action="" method="POST">
				<div class="form-group">
					<label for="datetime">Date/Time</label>
					<div class='input-group date' id='datetimepicker1'>
						<input type="text" class="form-control" name="datetime" id="datetime" placeholder="Date/Time" data-date-format="YYYY/MM/DD HH:mm:ss">
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</div>
				<script type="text/javascript">
					$(function () {
						$('#datetimepicker1').datetimepicker();
					});
				</script>
				<div class="form-group">
					<label for="company">Company</label>
					<?php
						$getCompanies = mysql_query('SELECT * FROM company WHERE status = 1 ORDER BY name ASC');
						if(mysql_num_rows($getCompanies) > 0) {
							echo '<select name="company" id="company" class="form-control">';
							echo '<option>Please Select</option>';
							while($company = mysql_fetch_assoc($getCompanies)) {
								echo '<option ';
								if(isset($GLOBALS['companyid']) && $GLOBALS['companyid'] == $company['id']) {
									echo 'SELECTED ';
								}
								echo 'value="'.$company['id'].'">'.$company['name'].'</option>';
							}
							echo '</select>';
						}
					?>
				</div>
				<div class="form-group">
					<label for="division">Division</label>
					<?php
						if(isset($GLOBALS['companyid'])) {
							$getDivisions = mysql_query('SELECT * FROM division WHERE company = '.$GLOBALS['companyid'].' AND status = 1 ORDER BY divid ASC');
						} else {
							$getDivisions = mysql_query('SELECT * FROM division WHERE status = 1 ORDER BY divid ASC');
						}
						if(mysql_num_rows($getDivisions) > 0) {
							echo '<select name="division" id="division" class="form-control">';
							echo '<option>Please Select</option>';
							while($division = mysql_fetch_assoc($getDivisions)) {
								echo '<option ';
								if(isset($GLOBALS['divisionid']) && $GLOBALS['divisionid'] == $division['id']) {
									echo 'SELECTED ';
								}
								echo 'value="'.$division['id'].'">'.$division['divid'].' - '.$division['name'].'</option>';
							}
							echo '</select>';
						} else {
							echo '<p class="padding15 bg-danger text-danger">No Associated Divisions.</p>';
						}
					?>
				</div>
				<div class="form-group">
					<label for="unit">Unit</label>
					<?php
						if(isset($GLOBALS['unitid'])) {
							$getUnits = mysql_query('SELECT * FROM unit WHERE division = '.$GLOBALS['divisionid'].' AND status = 1 ORDER BY id ASC');
						} else {
							$getUnits = mysql_query('SELECT * FROM unit WHERE status = 1 ORDER BY id ASC');
						}
						if(mysql_num_rows($getUnits) > 0) {
							echo '<select name="unit" id="unit" class="form-control">';
							echo '<option>Please Select</option>';
							while($unit = mysql_fetch_assoc($getUnits)) {
								echo '<option ';
								if(isset($GLOBALS['unitid']) && $GLOBALS['unitid'] == $unit['id']) {
									echo 'SELECTED ';
								}
								echo 'value="'.$unit['id'].'">'.$unit['identnum'].'</option>';
							}
							echo '</select>';
						} else {
							echo '<p class="padding15 bg-danger text-danger">No Associated Unit.</p>';
						}
					?>
				</div>
				<div class="form-group">
					<label for="equipment">Equipment</label>
					<select name="equipment" id="equipment" class="form-control">
						<option>Please Select</option>
					</select>
				</div>
				<div class="form-group">
					<label for="title">Title</label>
					<input type="text" class="form-control" name="title" id="title" placeholder="Searchable title for log entry">
				</div>
				<div class="form-group">
					<label for="contact">Contact</label>
					<input type="text" class="form-control" name="contact" id="contact" placeholder="Contact (ex. Caller, Emailer, On-site Contact)">
				</div>
				<div class="form-group">
					<label for="type">Log Type</label>
					<select name="type" id="type" class="form-control">
						<option>Please Select</option>
						<option value="1">Phone</option>
						<option value="2">Email</option>
						<option value="3">On-Site</option>
						<option value="4">Off-Site</option>
						<option value="5">Headquarters</option>
					</select>
				</div>
				<div class="form-group">
					<label for="quicknote">Question/Issue</label>
					<select name="quicknote" id="quicknote" class="form-control">
						<option>Please Select</option>
						<option value="1">Generator Problems</option>
						<option value="2">Reactor Problems</option>
						<option value="3">Compressor Problems</option>
						<option value="4">Material Problems</option>
						<option value="5">Building Science</option>
						<option value="6">Other</option>
					</select>
				</div>
				<div class="form-group">
					<label for="notes">Notes</label>
					<textarea class="form-control" rows="6" name="notes"  id="notes" placeholder="Add Notes"></textarea>
				</div>
				<button type="submit" name="addLog" class="btn btn-default">Submit</button>
			</form>
		<?
	}