<h1>Login</h1>
<p>Welcome to the Spray Foam Systems CloudPAK, please login below to access the system. If you do not have a login yet, please <a href="">register</a>. Once you have been approved for access you will be able to access this site.</p>
<?php
	$loginHash = sha1(md5(time()));
?>
<form action="/login/check/<?php echo $loginHash; ?>/" method="POST">
	<div class="form-group">
		<label for="email">Email Address</label>
		<input type="text" class="form-control" name="email" id="email" placeholder="Email Address">
	</div>
	<div class="form-group">
		<label for="password">Password</label>
		<input type="password" class="form-control" name="password" id="password" placeholder="Password">
	</div>
	<input type="hidden" name="secondaryPass" value="24d5y795mf5a26s5g52t54d" />
	<input type="hidden" name="computerPin" value="<?php echo $loginHash; ?>" />
	<button type="submit" name="login" class="btn btn-default">Submit</button>
</form>