<?php
	function title() {
		echo 'Add New Log';
	}
	
	function content() {
		?>
			<h1>Add New Lead</h1>
			<p>Fill out the form below and submit to add a new lead.</p>
			<?php
				if(isset($_POST['addLead'])) {
					
					if(mysql_query('
						INSERT INTO log (
							id,
							time,
							company,
							division,
							unit,
							equipment,
							title,
							contact,
							type,
							quicknote,
							notes
						) VALUES (
							0,
							"'.strtotime($_POST['datetime']).'",
							"'.$_POST['company'].'",
							"'.$_POST['division'].'",
							"'.$_POST['unit'].'",
							"'.$_POST['equipment'].'",
							"'.$_POST['title'].'",
							"'.$_POST['contact'].'",
							"'.$_POST['type'].'",
							"'.$_POST['quicknote'].'",
							"'.$_POST['notes'].'"
						)
					')) {
						echo '<p class="padding15 bg-success text-success">You have sucessfully added a new log.</p>';
					} else {
						echo '<p class="padding15 bg-danger text-danger">The log wasn\'t added, something went wrong, talk to IT Brett.<br />'.mysql_error().'</p>';
					}
				}
			?>
			<form action="" method="POST">
				<div class="form-group">
					<label for="contact">Contact Name</label>
					<input type="text" class="form-control" name="contact" id="contact" placeholder="Contact Name">
				</div>
				<div class="form-group">
					<label for="company">Company</label>
					<input type="text" class="form-control" name="company" id="company" placeholder="Company Name">
				</div>
				<div class="form-group">
					<label for="phone">Phone Number</label>
					<input type="text" class="form-control" name="phone" id="phone" placeholder="Phone Number">
				</div>
				<div class="form-group">
					<label for="email">Email Address</label>
					<input type="text" class="form-control" name="email" id="email" placeholder="Email Address">
				</div>
				<div class="form-group">
					<label for="address">Address</label>
					<input type="text" class="form-control" name="address" id="address" placeholder="Address">
				</div>
				<div class="form-group">
					<label for="address2">Address 2</label>
					<input type="text" class="form-control" name="address2" id="address2" placeholder="Address 2">
				</div>
				<div class="form-group">
					<label for="city">City</label>
					<input type="text" class="form-control" name="city" id="city" placeholder="City">
				</div>
				<div class="form-group">
					<label for="state">State</label>
					<input type="text" class="form-control" name="state" id="state" placeholder="State">
				</div>
				<div class="form-group">
					<label for="zip">Zip Code</label>
					<input type="text" class="form-control" name="zip" id="zip" placeholder="Zip Code">
				</div>
				<div class="form-group">
					<label for="notes">Notes</label>
					<textarea class="form-control" rows="6" name="notes"  id="notes" placeholder="What is the lead looking for? Rig? Standalone system? Equipment?"></textarea>
				</div>
				<button type="submit" name="addLead" class="btn btn-default">Submit</button>
			</form>
		<?
	}