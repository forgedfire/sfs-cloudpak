<?php
	function title() {
		echo 'Sales Orders';
	}
	
	function content() {
		?>
			<h1>Sales Orders <a class="btn btn-default" href="/add/lead/" role="button">Add</a></h1>
			<p>View orders taken via phone, email, or in office.</p>
			<h2>Orders</h2>
			<div class="row">
				<div class="col-lg-6">
					<h3>Unpaid Orders</h3>
					<p class="padding15 bg-danger text-danger">No unpaid orders.</p>
				</div>
				<div class="col-lg-6">
					<h3>Orders not shipped</h3>
					<p class="padding15 bg-danger text-danger">No unshipped orders.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<h3>Orders within last 30 days</h3>
					<p class="padding15 bg-danger text-danger">No recent orders.</p>
				</div>
			</div>
		<?
	}