<?php
	function title() {
		echo 'Add New User';
	}
	
	function content() {
		?>
			<h1>Add New User</h1>
			<p>Fill out the form below and submit to add a new user.</p>
			<?php
				if(isset($_POST['addLog'])) {
					if(mysql_query('
						INSERT INTO user (
							id,
							status,
							email,
							password,
							firstname,
							lastname,
							office,
							cell
						) VALUES (
							0,
							1,
							"'.$_POST['email'].'",
							"'.md5($_POST['password']).'",
							"'.$_POST['firstname'].'",
							"'.$_POST['lastname'].'",
							"'.$_POST['office'].'",
							"'.$_POST['cell'].'"
						)
					')) {
						echo '<p class="padding15 bg-success text-success">You have sucessfully added a new user.</p>';
					} else {
						echo '<p class="padding15 bg-danger text-danger">The user wasn\'t added, something went wrong, talk to IT Brett.<br />'.mysql_error().'</p>';
					}
				}
			?>
			<form action="" method="POST">
				<div class="form-group">
					<label for="email">Email Address</label>
					<input type="text" class="form-control" name="email" id="email" placeholder="Email Address">
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" class="form-control" name="password" id="password" placeholder="Password">
				</div>
				<div class="form-group">
					<label for="firstname">First Name</label>
					<input type="text" class="form-control" name="firstname" id="firstname" placeholder="First Name">
				</div>
				<div class="form-group">
					<label for="lastname">Last Name</label>
					<input type="text" class="form-control" name="lastname" id="lastname" placeholder="Last Name">
				</div>
				<div class="form-group">
					<label for="office">Office #</label>
					<input type="text" class="form-control" name="office" id="office" placeholder="Office #">
				</div>
				<div class="form-group">
					<label for="cell">Cell #</label>
					<input type="text" class="form-control" name="cell" id="cell" placeholder="Cell #">
				</div>
				<button type="submit" name="addLog" class="btn btn-default">Submit</button>
			</form>
		<?
	}