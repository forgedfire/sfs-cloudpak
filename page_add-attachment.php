<?php
	global $type,$id;
	$type = $_GET['type'];
	$id = $_GET['id'];

	function title() {
		echo 'Add New Attachment';
	}
	
	function content() {
		?>
			<h1>Add New Attachment</h1>
			<p>Fill out the form below and submit to add a new attachment.</p>
			<?php
				global $type,$id;
				if(isset($_POST['addAttachment'])) {
					if(isset($_FILES["fileupload"])) {
						if($_FILES["fileupload"]["error"] > 0) {
							echo '<p class="padding15 bg-danger text-danger">There was an error uploading your file. Please copy the error below and send it you IT Brett.<br /><br /><strong>ERROR:</strong> '.$_FILES["fileupload"]["error"].'</p>';
						} else {
							$acceptedFiles = array(
								'application/vnd.ms-excel',                                                    // CSV         .csv
								'text/csv',                                                                    // CSV         .csv
								'text/plain',                                                                  // TXT         .txt
								'application/zip',                                                             // ZIP         .zip
								'application/x-7z-compressed',                                                 // 7Z          .7z
								'application/x-tar',                                                           // TAR         .tar
								'application/pdf',                                                             // PDF         .pdf
								'application/epub+zip',                                                        // EPUB        .epub
								'message/rfc822',                                                              // Email       .eml
								'text/html',                                                                   // HTML        .html
								'image/jpeg',                                                                  // JPEG        .jpg .jpeg
								'image/png',                                                                   // PNG         .png
								'image/tiff',                                                                  // TIFF        .tiff
								'image/bmp',                                                                   // BMP         .bmp
								'application/msword',                                                          // WORD        .doc
								'application/vnd.openxmlformats-officedocument.wordprocessingml.document',     // WORD        .docx
								'application/vnd.ms-excel',                                                    // EXCEL       .xls
								'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',           // EXCEL       .xlsx
								'application/vnd.ms-powerpoint',                                               // Powerpoint  .ppt
								'application/vnd.openxmlformats-officedocument.presentationml.presentation',   // Powerpoint  .pptx
								'application/x-mspublisher',                                                   // Publisher   .pub
								'text/x-vcard',                                                                // VCard       .vcf
								'video/quicktime'                                                              // MOV         .mov
							);
							
							if(in_array($_FILES["fileupload"]["type"],$acceptedFiles)) {
								$_FILES["fileupload"]["name"] = str_replace("/","",str_replace(" ","-",$_POST['name'])).'-'.time();
								switch($_FILES["fileupload"]["type"]) {
									case 'text/csv': $fileEXT = '.csv'; $filetype = '4';
										break;
									case 'text/plain': $fileEXT = '.txt'; $filetype = '4';
										break;
									case 'application/zip': $fileEXT = '.zip'; $filetype = '6';
										break;
									case 'application/x-7z-compressed': $fileEXT = '.7z'; $filetype = '6';
										break;
									case 'application/x-tar': $fileEXT = '.tar'; $filetype = '6';
										break;
									case 'application/pdf': $fileEXT = '.pdf'; $filetype = '1';
										break;
									case 'application/epub+zip': $fileEXT = '.epub'; $filetype = '0';
										break;
									case 'message/rfc822': $fileEXT = '.eml'; $filetype = '0';
										break;
									case 'text/html': $fileEXT = '.html'; $filetype = '0';
										break;
									case 'image/jpeg': $fileEXT = '.jpg'; $filetype = '5';
										break;
									case 'image/png': $fileEXT = '.png'; $filetype = '5';
										break;
									case 'image/tiff': $fileEXT = '.tiff'; $filetype = '5';
										break;
									case 'image/bmp': $fileEXT = '.bmp'; $filetype = '5';
										break;
									case 'application/msword': $fileEXT = '.doc'; $filetype = '3';
										break;
									case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document': $fileEXT = '.docx'; $filetype = '3';
										break;
									case 'application/vnd.ms-excel': $fileEXT = '.xls'; $filetype = '2';
										break;
									case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': $fileEXT = '.xlsx'; $filetype = '2';
										break;
									case 'application/vnd.ms-powerpoint': $fileEXT = '.ppt'; $filetype = '8';
										break;
									case 'application/vnd.openxmlformats-officedocument.presentationml.presentation': $fileEXT = '.pptx'; $filetype = '8';
										break;
									case 'application/x-mspublisher': $fileEXT = '.pub'; $filetype = '0';
										break;
									case 'text/x-vcard': $fileEXT = '.vcf'; $filetype = '0';
										break;
									case 'video/quicktime': $fileEXT = '.mov'; $filetype = '7';
										break;
								}
								$filelocNme = '/uploads/'.$_FILES["fileupload"]["name"].$fileEXT;
								if(move_uploaded_file($_FILES["fileupload"]["tmp_name"], $_SERVER["DOCUMENT_ROOT"].$filelocNme)) {
									if(mysql_query('INSERT INTO attachment (id,infoTypeLink,attachedTo,filetype,name,fileLocation) VALUES (0,'.$type.','.$id.','.$filetype.',"'.$_POST['name'].'","'.$filelocNme.'")')) {
										echo '<p class="padding15 bg-success text-success">You have sucessfully added a new attachment to the system</p>';
									} else {
										echo '<p class="padding15 bg-danger text-danger">The attachment wasn\'t uploaded, something went wrong, talk to IT Brett.</p>';
									}
								} else {
									echo '<p class="padding15 bg-danger text-danger">Failed to move file from temporary location to final destination.</p>';
								}
							} else {
								echo '<p class="padding15 bg-danger text-danger">You are not allowed to upload that type of file.</p>';
							}
						}
					} else {
						echo '<p class="padding15 bg-danger text-danger">You didn\'t attach a file, please try again.</p>';
					}
				}
			?>
			<form action="" method="POST" enctype="multipart/form-data">
				<div class="form-group">
					<label for="name">Attachment Title</label>
					<input type="text" class="form-control" name="name"  id="name" placeholder="Attachment Title">
				</div>
				<div class="form-group">
					<label for="fileupload">Upload File</label>
					<input type="file" id="fileupload" name="fileupload">
				</div>
				<button type="submit" name="addAttachment" class="btn btn-default">Submit</button>
			</form>
		<?
	}