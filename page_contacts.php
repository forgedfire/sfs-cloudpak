<?php
	function title() {
		echo 'Contacts';
	}
	
	function content() {
		?>
			<h1>Contacts</h1>
            <p>Use the search box below to find contacts that you are looking for. If you can't find the contact, it may not exist, or you don't have permission to view their details.
			<?php
				$getContacts = mysql_query('SELECT * FROM contact WHERE status = 1');
				if(mysql_num_rows($getContacts) > 0) {
					while($contact = mysql_fetch_assoc($getContacts)) {
						echo '<a class="btn btn-default" href="#" role="button">'.$contact['lastname'].', '.$contact['firstname'].'</a>';
					}
				} else {
					echo '<p class="padding15 bg-danger text-danger">No visible contacts.</p>';
				}
			?>
		<?
	}