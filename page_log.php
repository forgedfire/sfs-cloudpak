<?php
	$getLog = mysql_query('SELECT * FROM log WHERE id = '.$_GET['id'].'');
	if(mysql_num_rows($getLog) > 0) {
		global $log;
		$log = mysql_fetch_assoc($getLog);
	} else {
		global $retrievalError;
		$retrievalError = true;
	}

	function title() {
		global $log,$retrievalError;
		if($retrievalError == true) {
			echo '404 Error';
		} else {
			echo 'Log: '.date('m/d/y g:i A',$log['time']).' - ';
			switch ($log['quicknote']) {
				case 1: echo 'Generator Problems';
				break;
				case 2: echo 'Reactor Problems';
				break;
				case 3: echo 'Compressor Problems';
				break;
				case 4: echo 'Material Problems';
				break;
				case 5: echo 'Building Science';
				break;
				case 6: echo 'Other';
				break;
			}
		}
	}
	
	function content() {
		global $log,$retrievalError;
		if($retrievalError == true) {
			echo '<p class="padding15 bg-danger text-danger">You reached a bad page, please go back and try again.</p>';
		} else {
			?>
				<h1>Log: <?php echo date('m/d/y g:i A',$log['time']).' - '; ?>
					<?php
						switch ($log['quicknote']) {
							case 1: echo 'Generator Problems';
							break;
							case 2: echo 'Reactor Problems';
							break;
							case 3: echo 'Compressor Problems';
							break;
							case 4: echo 'Material Problems';
							break;
							case 5: echo 'Building Science';
							break;
							case 6: echo 'Other';
							break;
						}
					?>
				</h1>
				<div class="row">
					<div class="col-lg-4">
						<h2>Log Info</h2>
						<h3>Company</h3>
							<?php
								if($log['company'] == 0) {
									echo '<p class="padding15 bg-danger text-danger">Log is not assigned to a company.</p>';
								} else {
									$company = mysql_fetch_assoc(mysql_query('SELECT name FROM company WHERE id = '.$log['company'].''));
									echo '<p>'.$company['name'].'</p>';
								}
							?>
						<h3>Division</h3>
							<?php
								if($log['division'] == 0) {
									echo '<p class="padding15 bg-danger text-danger">Log is not assigned to a division.</p>';
								} else {
									$division = mysql_fetch_assoc(mysql_query('SELECT name FROM division WHERE id = '.$log['division'].''));
									echo '<p>'.$division['name'].'</p>';
								}
							?>
						<h3>Unit</h3>
							<?php
								if($log['unit'] == 0) {
									echo '<p class="padding15 bg-danger text-danger">Log is not assigned to a setup.</p>';
								} else {
									$unit = mysql_fetch_assoc(mysql_query('SELECT identnum FROM unit WHERE id = '.$log['unit'].''));
									echo '<p>'.$unit['identnum'].'</p>';
								}
							?>
						<h3>Equipment</h3>
							<?php
								if($log['equipment'] == 0) {
									echo '<p class="padding15 bg-danger text-danger">Log is not assigned to a equipment.</p>';
								} else {
									$equipment = mysql_fetch_assoc(mysql_query('SELECT name FROM equipment WHERE id = '.$log['equipment'].''));
									echo '<p>'.$equipment['name'].'</p>';
								}
							?>
						<h3>Contact</h3>
						<p><?php echo $log['contact']; ?></p>
					</div>
					<div class="col-lg-8">
						<h2>Notes</h2>
						<h3>Question/Issue</h3>
						<p>
							<?php
								switch($log['quicknote']) {
									case 1: echo 'Generator Problems';
									break;
									case 2: echo 'Reactor Problems';
									break;
									case 3: echo 'Compressor Problems';
									break;
									case 4: echo 'Material Problems';
									break;
									case 5: echo 'Building Science';
									break;
									case 6: echo 'Other';
									break;
								}
							?>
						</p>
						<h3>Detailed Notes</h3>
						<p><?php echo $log['notes']; ?></p>
						<h3>Attachments <a class="btn btn-default" href="/add/attachment/log/<?php echo $log['id']; ?>/" role="button">Add</a></h3>
						<?php
							$getAttachments = mysql_query('SELECT * FROM attachment WHERE infoTypeLink = 5 AND attachedTo = '.$log['id'].'');
							if(mysql_num_rows($getAttachments) > 0) {
								while($attachment = mysql_fetch_assoc($getAttachments)) {
									echo '<a title="'.$attachment['name'].'" class="attach-button btn btn-default" href="'.$attachment['fileLocation'].'" role="button" target="_blank">';
										switch($attachment['filetype']) {
											case 0: echo '<i class="fa fa-file"></i>';
											break;
											case 1: echo '<i class="fa fa-file-pdf-o"></i>';
											break;
											case 2: echo '<i class="fa fa-file-excel-o"></i>';
											break;
											case 3: echo '<i class="fa fa-file-word-o"></i>';
											break;
											case 4: echo '<i class="fa fa-file-text-o"></i>';
											break;
											case 5: echo '<i class="fa fa-file-image-o"></i>';
											break;
											case 6: echo '<i class="fa fa-file-archive-o"></i>';
											break;
											case 7: echo '<i class="fa fa-file-video-o"></i>';
											break;
											case 8: echo '<i class="fa fa-file-powerpoint-o"></i>';
											break;
											case 9: echo '<i class="fa fa-file-audio-o"></i>';
											break;
										}
									echo $attachment['name'].'</a>';
								}
							} else {
								echo '<p class="padding15 bg-danger text-danger">This log has no attachments.</p>';
							}
						?>
					</div>
				</div>
			<?
		}
	}