<?php
	global $getUser;
	$getUser = mysql_query('SELECT status,email,firstname,lastname,office,cell FROM user WHERE id = '.$_GET['id'].'');
	if(mysql_num_rows($getUser) == 1) {
		$error = false;
		$getUser = mysql_fetch_assoc($getUser);
	} else {
		$error = true;
	}

	function title() {
		echo 'Manage User';
	}
	
	function addToHead() {
	?>
		<link href="/css/bootstrap-switch.min.css" rel="stylesheet">
		<script src="/js/bootstrap-switch.min.js"></script>
		
	<?
	}
	
	function content() {
		if($error == false) { 
		global $getUser;
		?>
			<h1>Manage User</h1>
			<div class="row">
				<div class="col-lg-6">
					<h3>User Information <input type="checkbox" name="enabled"<?php if($getUser['status'] == 1) { echo ' checked'; } ?>></h3>
					<h4><?php echo $getUser['lastname'].', '.$getUser['firstname']; ?></h4>
					<p>Email: <?php echo $getUser['email']; ?><br />Office: <?php echo $getUser['office']; ?><br />Cell: <?php echo $getUser['cell']; ?></p>
					<script> $("[name='enabled']").bootstrapSwitch(); </script>
				</div>
				<div class="col-lg-6">
					<h3>User Permissions</h3>
					<p class="padding15 bg-danger text-danger">No permissions available.</p>
				</div>
			</div>
		<?
		} else {
		?>
			<h1>Error, no such user</h1>
			<p>Sorry there is no record of that user, please go back and try again.</p>
		<?
		}
	}